﻿using OneUp.OneUpMonitor.Logic;
using OneUp.Shared.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor.Monitors
{
    public class CFLMonitor
    {
        CFLLogic cflLogic = new CFLLogic();

        public async Task RunAsync()
        {
            Log.Instance().InfoFormat("Starting CFL Monitor Async...");

            await Task.Run(() => Run());

            Log.Instance().InfoFormat("Shutting down CFL Monitor Async");
        }

        public void Run()
        {

        }
    }
}
