﻿using OneUp.OneUpMonitor.EmailModels.MLB;
using OneUp.OneUpMonitor.Logic;
using OneUp.OneUpMonitor.Models;
using OneUp.Shared.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor.Monitors
{
    public class MLBMonitor
    {
        MLBLogic logic = new MLBLogic();
        MLBEmails emails = new MLBEmails();
        CommonLogic commonLogic = new CommonLogic("MLBMonitor");
        Double maxAllowedDeltaBetweenPlays = Convert.ToInt32(ConfigurationManager.AppSettings["mlb.maxAllowedDeltaBetweenPlays"]);
        Double maxAllowedDeltaInEventProcessing = Convert.ToInt32(ConfigurationManager.AppSettings["mlb.maxAllowedDeltaInEventProcessing"]);
        private int dataProcessingRestartCount = 0;
        private int eventServiceRestartCount = 0;
        int updateEmailInterval = int.Parse(ConfigurationManager.AppSettings["mlb.UpdateEmailInterval"]);
        Boolean keepRunning = true;

        //Games that fail to start or end will be added here so that multiple emails won't be sent about it
        List<Schedule> failedGames = new List<Schedule>();

        public async Task RunAsync(int threadSleep)
        {
            String methodName = "RunAsync";
            commonLogic.Logger(Severity.Info, methodName, "Starting MLB Monitor Async...");

            await Task.Run(() => Run(threadSleep));

            commonLogic.Logger(Severity.Info, methodName, "Shutting down MLB Monitor Async");
        }

        public void Run(int threadSleep)
        {
            String methodName = "Run";
            DateTime nextMLBGameStartEndCheck = DateTime.MinValue;
            DateTime nextMLBUpdateEmail = DateTime.MinValue;
            DateTime nextMLBDeltaCheck = DateTime.MinValue;

            while (keepRunning)
            {
                //Monitor MLB if enabled and after the next run time
                if (Boolean.Parse(ConfigurationManager.AppSettings["mlb.Enabled"]) == true)
                {
                    //If any deltas are exceeded, this is set to true. delayBetweenRuns is set based on this variable
                    Boolean deltasExceeded = false;

                    if (DateTime.Now > nextMLBGameStartEndCheck)
                    {
                        //Check that games started and ended in a timely manner
                        nextMLBGameStartEndCheck = CheckGameStartAndEndTimes();

                        //Initialize the next update email to 20 min prior to the gamecheck
                        if (nextMLBUpdateEmail == DateTime.MinValue)
                        {
                            nextMLBUpdateEmail = commonLogic.GetNextInterval(nextMLBGameStartEndCheck, updateEmailInterval).Subtract(new TimeSpan(0, 30, 0)); ;// nextMLBGameCheck.Subtract(new TimeSpan(0, 20, 0));
                            commonLogic.Logger(Severity.Info, methodName, "Initializing next MLB update email to {0}", nextMLBUpdateEmail.ToLongTimeString());
                        }

                        if (nextMLBDeltaCheck == DateTime.MinValue)
                        {
                            nextMLBDeltaCheck = nextMLBGameStartEndCheck;
                            commonLogic.Logger(Severity.Info, methodName, "Initializing next MLB delta check to {0}", nextMLBDeltaCheck.ToLongTimeString());
                        }
                    }

                    if (DateTime.Now > nextMLBDeltaCheck)
                    {
                        //Check deltas
                        List<Play> latestPlays = new List<Play>();
                        deltasExceeded = CheckPlayDeltas(out latestPlays);
                        deltasExceeded = CheckGameEventDeltas(latestPlays);

                        if (deltasExceeded)
                        {
                            //If deltas were exceeded, set next run time to 10 minutes
                            commonLogic.Logger(Severity.Info, methodName, "Deltas exceeded, MLB services were restarted.");
                        }
                        else
                        {
                            //Else run again in the time specified in app.config
                            commonLogic.Logger(Severity.Info, methodName, "MLB Deltas are valid or no games are in progress.");
                        }

                        nextMLBDeltaCheck = commonLogic.GetNextInterval(5);
                        commonLogic.Logger(Severity.Info, methodName, "Next MLB Delta Check is at {0}", nextMLBDeltaCheck.ToLongTimeString());
                    }

                    if (Boolean.Parse(ConfigurationManager.AppSettings["mlb.EnableUpdateEmails"]))
                    {
                        if (DateTime.Now > nextMLBUpdateEmail)
                        {
                            //Check that games started and ended in a timely manner
                            nextMLBUpdateEmail = HandleUpdateEmail();
                        }
                    }
                }

                System.Threading.Thread.Sleep(threadSleep * 1000);
            }
        }

        /// <summary>
        /// Checks that games have started and ended properly and sends emails upon failure or restart. Returns a DateTime of the next scheduled run time.
        /// </summary>
        /// <returns></returns>
        public DateTime CheckGameStartAndEndTimes()
        {
            String methodName = "CheckGameStartAndEndTimes";
            //Get games with a start date of today's date
            commonLogic.Logger(Severity.Info, methodName, "");
            commonLogic.Logger(Severity.Info, methodName, "##--Checking today's games for valid start and end time...--##");
            commonLogic.Logger(Severity.Info, methodName, "Getting today's games...");
            List<Schedule> todaysGames = logic.GetGamesByDay(DateTime.Now).ToList();
            commonLogic.Logger(Severity.Info, methodName, "Done.");

            DateTime currentDateTime = DateTime.Now;
            foreach (Schedule game in todaysGames)
            {
                //Download the Plays XML for this game
                //Check if current date time is within a few minutes of the game's StartDateTime because the Plays XML is not available until a few min before the game

                //Retreive the plays XML
                commonLogic.Logger(Severity.Info, methodName, "Getting Plays XML for GameCode {0}", game.GameCode);
                String playsXml = logic.GetPlaysXML(game.ProviderGameID, game.StartDateTime);
                commonLogic.Logger(Severity.Info, methodName, "Plays XML Retrieved");

                //If it's null, then the playsXml may not be available
                if (!string.IsNullOrWhiteSpace(playsXml))
                {
                    //Get the game status from the XML
                    String xmlGameStatus = logic.GetGameStatus(playsXml);

                    //If current time is after the game's start time and the game status is in progress and the game in the DB is still in pre-game, send email
                    if (currentDateTime > game.StartDateTime.AddMinutes(1) &&
                        xmlGameStatus.ToLower() == "in-progress" &&
                        game.Status.ToLower() == "pre-game" &&
                        !failedGames.Exists(i => i.GameCode == game.GameCode))
                    {
                        //Game failed to start
                        commonLogic.Logger(Severity.Info, methodName, "Game {0} failed to start, sending email.", game.GameCode);

                        //Send email
                        emails.SendGameFailedToStartEmail(game);

                        //Add to failed games list
                        failedGames.Add(game);
                    }
                    else if (currentDateTime > game.StartDateTime.AddMinutes(1) &&
                        xmlGameStatus.ToLower() == "in-progress" &&
                        game.Status.ToLower() == "in-progress" &&
                        failedGames.Exists(i => i.GameCode == game.GameCode))
                    {
                        //Game successfully started after having failed to start
                        commonLogic.Logger(Severity.Info, methodName, "Game {0} started, sending email.", game.GameCode);

                        //Send email
                        emails.SendGameStartedEmail(game);

                        //Remove from failed games list
                        failedGames.RemoveAll(i => i.GameCode == game.GameCode);
                    }

                    //If the XML shows a status of Final and the game in the DB is not Final, send an email
                    if (xmlGameStatus.ToLower() == "final" &&
                        game.Status.ToLower() == "in-progress" &&
                        !failedGames.Exists(i => i.GameCode == game.GameCode))
                    {
                        //Game failed to end
                        commonLogic.Logger(Severity.Info, methodName, "Game {0} failed to end, sending email.", game.GameCode);

                        DateTime failedTime = DateTime.Now;

                        //Sleep for one minute and check the DB again to make sure that it hasn't been marked yet
                        System.Threading.Thread.Sleep(60000);

                        //Get the game again
                        Schedule theGameToCheck = logic.GetGame(game.GameCode);

                        //If the game status still isn't final, send an email
                        if (theGameToCheck.Status.ToLower() != "final")
                        {
                            //Send email
                            emails.SendGameFailedToEndEmail(game, failedTime);
                        }
                    }
                    else if (xmlGameStatus.ToLower() == "final" &&
                        game.Status.ToLower() == "final" &&
                        failedGames.Exists(i => i.GameCode == game.GameCode))
                    {
                        //Game ended successfully
                        commonLogic.Logger(Severity.Info, methodName, "Game {0} ended, sending email.", game.GameCode);

                        //Send email
                        emails.SendGameEndedEmail(game);

                        //Remove from failed games list
                        failedGames.RemoveAll(i => i.GameCode == game.GameCode);
                    }
                }
            }

            //Set next run time
            DateTime nextRunTime = DateTime.MinValue;
            DateTime firstGameTime = todaysGames.Min(i => i.StartDateTime);
            DateTime firstGameTimeMinus10 = firstGameTime.Subtract(new TimeSpan(0, 10, 0));;
            int preGameCount = todaysGames.Count(i => i.Status.ToLower() == "pre-game");
            int finalCount = todaysGames.Count(i => i.Status.ToLower() == "final");

            if (preGameCount == todaysGames.Count() && DateTime.Now > firstGameTimeMinus10)
            {
                //If all games are in pregame but it's past the 10 minutes prior mark, set the next run time to the first game's start
                if(DateTime.Now < firstGameTime)
                {
                    nextRunTime = firstGameTime;
                }
                else
                {
                    nextRunTime = DateTime.Now.AddMinutes(10);
                }
                commonLogic.Logger(Severity.Info, methodName, "Games are in pre-game, but games haven't started. Next game start/end check time is {0}", nextRunTime.ToLongTimeString());
            }
            else if (preGameCount == todaysGames.Count())
            {
                //If all games are set to pre-game, set the next run to 10 minutes prior to the start of the next game
                nextRunTime = firstGameTimeMinus10;
                commonLogic.Logger(Severity.Info, methodName, "All of today's games are currently in Pre-Game. Next game start/end check time is {0}", nextRunTime.ToLongTimeString());
            }
            else if(todaysGames.Exists(i => i.Status.ToLower() == "in-progress"))
            {
                //If any games are in progress, run every five minutes
                nextRunTime = DateTime.Now.AddMinutes(5);
                commonLogic.Logger(Severity.Info, methodName, "Games are in progress. Next game start/end check time is {0}", nextRunTime.ToLongTimeString());
            }
            else if (finalCount == todaysGames.Count())
            {
                //If all games are in final, set next run time to 5 AM the next day
                if(DateTime.Now.TimeOfDay < TimeSpan.FromHours(4))
                {
                    nextRunTime = DateTime.Now.Date.AddHours(5);
                }
                else
                {
                    nextRunTime = DateTime.Now.Date.AddDays(1).AddHours(5);
                }
                commonLogic.Logger(Severity.Info, methodName, "All of today's games are finalized. Next game start/end check time is {0}", nextRunTime.ToLongTimeString());

                //If all games are final, send update email (if enabled)
                if(Boolean.Parse(ConfigurationManager.AppSettings["mlb.EnableUpdateEmails"]))
                {
                    HandleUpdateEmail();
                }
            }
            

            return nextRunTime;

        }

        public Boolean CheckGameEventDeltas(List<Play> latestPlays)
        {
            String methodName = "CheckGameEventDeltas";
            commonLogic.Logger(Severity.Info, methodName, "");
            commonLogic.Logger(Severity.Info, methodName, "##--Checking GameEvents for valid deltas...--##");
            Boolean areAllowedDeltasExceeded = false;
            int numOfExceededDeltas = 0;

            //If allowed deltas hasn't been exceeded yet, check the plays and the GameEvent table
            if (logic.AreGamesInProgress())
            {
                //Get the GameEvents
                commonLogic.Logger(Severity.Info, methodName, "Getting GameEvents...");
                List<GameEvent> gameEvents = new List<GameEvent>();
                foreach (Play play in latestPlays)
                {
                    commonLogic.Logger(Severity.Info, methodName, "Getting GameEvent for play ID {0}", play.Id);
                    GameEvent gameEvent = logic.GetGameEvent(play.Id);
                    if(gameEvent == null)
                    {
                        commonLogic.Logger(Severity.Info, methodName, "No GameEvent found for play ID {0}", play.Id);
                    }
                    else
                    {
                        gameEvents.Add(gameEvent);
                    }
                }
                commonLogic.Logger(Severity.Info, methodName, "Done.");

                //Reset the numOfExceededDeltas and check the Play.DateTimeCreated/GameEvent.ProcessingStarted and the start/end processing times of GameEvent
                commonLogic.Logger(Severity.Info, methodName, "Checking deltas on GameEvents...");
                foreach (GameEvent gameEvent in gameEvents)
                {
                    Play play = latestPlays.Where(i => i.Id == gameEvent.PlayID).Single();
                    double deltaForPlayAgainstGameEvent = logic.GetDelta(play, gameEvent);
                    double deltaForProcessingStartAndFinish = 0;// logic.GetDelta(gameEvent); //Not used at this moment

                    commonLogic.Logger(Severity.Info, methodName, "GameEvent ID: {0} / Play ID {1} / Delta: {2}", gameEvent.ID, play.Id, deltaForPlayAgainstGameEvent);

                    if (deltaForPlayAgainstGameEvent > maxAllowedDeltaInEventProcessing || deltaForProcessingStartAndFinish > maxAllowedDeltaInEventProcessing)
                    {
                        numOfExceededDeltas++;
                    }
                }
                commonLogic.Logger(Severity.Info, methodName, "Done.");

                //If all plays exceed the maximum allowed delta, kill the service and start it again
                if (numOfExceededDeltas >= gameEvents.Count() && numOfExceededDeltas > 2)
                {
                    commonLogic.Logger(Severity.Info, methodName, "GameEvent deltas exceeded, restarting EventService nodes...");

                    areAllowedDeltasExceeded = true;
                    StartKillTime skTime = commonLogic.RestartService("OneUp.Connect.MLB.EventService.Node1");
                    commonLogic.RestartService("OneUp.Connect.MLB.EventService.Node2");
                    commonLogic.RestartService("OneUp.Connect.MLB.EventService.Node3");
                    commonLogic.RestartService("OneUp.Connect.MLB.EventService.Node4");
                    commonLogic.RestartService("OneUp.Connect.MLB.EventService.Node5");
                    eventServiceRestartCount++;

                    //Email admin
                    commonLogic.Logger(Severity.Info, methodName, "Sending email");
                    emails.SendEventServiceNodesRestartedEmail(skTime);
                    commonLogic.Logger(Severity.Info, methodName, "Done.");
                }
                else
                {
                    commonLogic.Logger(Severity.Info, methodName, "GameEvent deltas are valid.");
                }
            }
            else
            {
                commonLogic.Logger(Severity.Info, methodName, "No games are in progress, skipping game event delta check");
            }

            return areAllowedDeltasExceeded;
        }

        /// <summary>
        /// Checks MLB services and returns true if any services had to be restarted
        /// </summary>
        /// <returns></returns>
        public Boolean CheckPlayDeltas(out List<Play> latestPlays)
        {
            String methodName = "CheckPlayDeltas";
            //Return value. Only set to true in the code below! 
            Boolean areAllowedDeltasExceeded = false;
            commonLogic.Logger(Severity.Info, methodName, "");
            commonLogic.Logger(Severity.Info, methodName, "##--Checking plays for valid deltas...--##");

            //Out variable that returns with the latest available plays in each game's innings.xml
            latestPlays = new List<Play>();

            //Check if games are in progress to prevent unnecessary runs
            if (logic.AreGamesInProgress())
            {
                #region Check Play Deltas

                //Get games in progress
                commonLogic.Logger(Severity.Info, methodName, "Getting games in progress...");
                List<Schedule> gamesInProgress = logic.GetGamesInProgress();
                commonLogic.Logger(Severity.Info, methodName, "Done.");

                //Get innings XML for each game
                commonLogic.Logger(Severity.Info, methodName, "Downloading innings XMLs for each game..");
                Dictionary<long, string> inningsXmls = new Dictionary<long, string>();
                foreach (Schedule game in gamesInProgress)
                {
                    inningsXmls.Add(game.GameCode, logic.GetInningsXml(game.ProviderGameID, game.StartDateTime));
                }
                commonLogic.Logger(Severity.Info, methodName, "Done.");

                //Get the last pitch from each XML
                commonLogic.Logger(Severity.Info, methodName, "Pulling the last pitch from each XML file...");
                List<Pitch> lastPitchOfEachGame = new List<Pitch>();
                foreach (KeyValuePair<long, string> inningsXml in inningsXmls)
                {
                    Pitch lastPitch = logic.GetLastPitchFromXML(inningsXml.Key, inningsXml.Value);
                    if(lastPitch != null)
                    {
                        lastPitchOfEachGame.Add(lastPitch);
                    }
                }
                commonLogic.Logger(Severity.Info, methodName, "Done.");

                //Get the latest play from each game in the DB
                commonLogic.Logger(Severity.Info, methodName, "Downloading the latest play for each game in the DB...");
                foreach (Pitch pitch in lastPitchOfEachGame)
                {
                    //Get the play
                    Play play = logic.GetPlay(pitch.GameCode, Convert.ToDecimal(pitch.Sequence));

                    //If play was found, add it to the list
                    if (play != null)
                    {
                        latestPlays.Add(play);
                    }

                }
                commonLogic.Logger(Severity.Info, methodName, "Done.");

                //Compare the delta between each play
                commonLogic.Logger(Severity.Info, methodName, "Comparing deltas between the plays in the XML and in the DB...");
                int numOfExceededDeltas = 0;
                foreach (Play play in latestPlays)
                {
                    //Get the delta using the play and the last pitch present in the XML
                    Pitch lastPitch = lastPitchOfEachGame.Where(i => i.GameCode == play.GameCode).First();
                    Double delta = logic.GetDelta(play, lastPitch);
                    String providerGameID = gamesInProgress.Where(i => i.GameCode == play.GameCode).First().ProviderGameID;
                    commonLogic.Logger(Severity.Info, methodName, String.Format("{0} / {1} / {2} : {3}", providerGameID, play.GameCode, play.Sequence, delta));
                    logic.LogDeltaToCSV(providerGameID, play.GameCode, play.Sequence, delta);
                    if (delta > maxAllowedDeltaBetweenPlays)
                    {
                        numOfExceededDeltas++;
                    }
                }
                commonLogic.Logger(Severity.Info, methodName, "");

                //If all plays exceed the maximum allowed delta, kill the service and start it again
                if (numOfExceededDeltas >= latestPlays.Count() && numOfExceededDeltas > 2)
                {
                    commonLogic.Logger(Severity.Info, methodName, "Plays table deltas exceeded, restarting DataProcessing service...");
                    
                    areAllowedDeltasExceeded = true;

                    //Restart the service
                    //StartKillTime skTime = commonLogic.RestartService("OneUp.DataProcessing.MLB");
                    StartKillTime skTime = new StartKillTime();
                    dataProcessingRestartCount++;

                    //Email admin
                    commonLogic.Logger(Severity.Info, methodName, "Sending email");
                    emails.SendDataProcessingRestartedEmail(skTime);
                }

                #endregion
            }
            else
            {
                commonLogic.Logger(Severity.Info, methodName, "No games are in progress, skipping play delta check");
            }

            return areAllowedDeltasExceeded;
        }

        /// <summary>
        /// Checks all game statuses and servers, sends an update email, and returns it's next scheduled run time
        /// </summary>
        /// <returns></returns>
        internal DateTime HandleUpdateEmail()
        {
            String methodName = "HandleUpdateEmail";
            commonLogic.Logger(Severity.Info, methodName, "");
            commonLogic.Logger(Severity.Info, methodName, "##--Handling update email...--##");
            commonLogic.Logger(Severity.Info, methodName, "Getting today's games");

            //Get today's games and filter the games that are in progress
            List<Schedule> todaysGames = logic.GetGamesByDay(DateTime.Now).OrderBy(i => i.StartDateTime).ToList();
            List<Schedule> gamesInProgress = todaysGames.Where(i => i.Status.ToLower() == "in-progress").ToList();

            //Get the last five plays for each game in progress to add to the email
            commonLogic.Logger(Severity.Info, methodName, "Getting plays from database for each in-progress game...");
            List<Play> plays = logic.GetLatestPlays(3);
            commonLogic.Logger(Severity.Info, methodName, "Plays retrieved.");

            //Check for valid deltas on plays and game events
            commonLogic.Logger(Severity.Info, methodName, "Running delta check...");
            List<Play> latestPlays = new List<Play>();
            Boolean isDataProcessingDeltaExceeded = CheckPlayDeltas(out latestPlays);
            Boolean isEventServicesDeltaExceeded = CheckGameEventDeltas(latestPlays);
            commonLogic.Logger(Severity.Info, methodName, "Delta check done.");

            //Get some game events for each play
            commonLogic.Logger(Severity.Info, methodName, "Getting GameEvents...");
            List<GameEvent> gameEvents = new List<GameEvent>();
            foreach (Play play in latestPlays)
            {
                commonLogic.Logger(Severity.Info, methodName, "Getting GameEvents for play {0}", play.Id);
                GameEvent gameEvent = logic.GetGameEvent(play.Id);
                if (gameEvent == null)
                {
                    commonLogic.Logger(Severity.Info, methodName, "No GameEvent found for play {0}", play.Id);
                }
                else
                {
                    gameEvents.Add(gameEvent);
                }
            }
            commonLogic.Logger(Severity.Info, methodName, "GameEvents retreived.");

            //Send email
            Update mlbUpdate = new Update()
            {
                LastFewGameEvents = gameEvents,
                LastFewPlays = plays,
                TodaysGames = todaysGames,
                DataProcessingRestartCount = dataProcessingRestartCount,
                EventServiceRestartCount = eventServiceRestartCount,
                IsDataProcessingDeltaExceeded = isDataProcessingDeltaExceeded,
                IsEventServicesDeltaExceeded = isEventServicesDeltaExceeded,
            };
            emails.SendUpdateEmail(mlbUpdate);

            //Schedule next run time
            //Set next run time
            DateTime nextRunTime = DateTime.MinValue;
            DateTime firstGameTime = todaysGames.Min(i => i.StartDateTime);
            DateTime firstGameTimeMinus30 = firstGameTime.Subtract(new TimeSpan(0, 30, 0));
            int preGameCount = todaysGames.Count(i => i.Status.ToLower() == "pre-game");
            int finalCount = todaysGames.Count(i => i.Status.ToLower() == "final");

            if (preGameCount == todaysGames.Count() && firstGameTimeMinus30 > DateTime.Now)
            {
                //If all games are set to pre-game, set the next run to 30 minutes prior to the start of the next game,
                //only if the current time is not within 30 min of the next game
                nextRunTime = commonLogic.GetNextInterval(firstGameTimeMinus30, updateEmailInterval);
                commonLogic.Logger(Severity.Info, methodName, "All of today's games are currently in Pre-Game. Next update email is {0}", nextRunTime.ToLongTimeString());
            }
            else if (todaysGames.Exists(i => i.Status.ToLower() == "in-progress"))
            {
                //If any games are in progress, run every 30 minutes
                
                nextRunTime = commonLogic.GetNextInterval(updateEmailInterval);

                commonLogic.Logger(Severity.Info, methodName, "Games are in progress. Next update email is {0}", nextRunTime.ToLongTimeString());
            }
            else if (finalCount == todaysGames.Count())
            {
                //If all games are in final, set next run time to 5 AM the next day
                if (DateTime.Now.TimeOfDay < TimeSpan.FromHours(4))
                {
                    nextRunTime = DateTime.Now.Date.AddHours(5);
                }
                else
                {
                    nextRunTime = DateTime.Now.Date.AddDays(1).AddHours(5);
                }
                commonLogic.Logger(Severity.Info, methodName, "All of today's games are finalized. Next update email is {0}", nextRunTime.ToLongTimeString());
            }
            else
            {
                if(DateTime.Now > firstGameTime)
                {
                    //If the current time is after the scheduled start of the first game but the game still hasn't started, 
                    //Set the next run time to the next 30 min.
                    nextRunTime = commonLogic.GetNextInterval(updateEmailInterval);
                    commonLogic.Logger(Severity.Info, methodName, "Scheduled games haven't started yet. Next update email is {0}", nextRunTime.ToLongTimeString());
                }
                else
                {
                    //Otherwise, set the time to the next interval
                    nextRunTime = commonLogic.GetNextInterval(firstGameTime, updateEmailInterval); ;
                    commonLogic.Logger(Severity.Info, methodName, "Next update email is {0}", nextRunTime.ToLongTimeString());
                }
            }

            return nextRunTime;
        }

        internal int GetEventServiceRestartCount()
        {
            return eventServiceRestartCount;
        }

        internal int GetDataProcessingRestartCount()
        {
            return dataProcessingRestartCount;
        }
    }

}
