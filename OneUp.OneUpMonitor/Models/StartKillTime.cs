﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor
{
    public class StartKillTime
    {
        public DateTime KillTime { get; set; }
        public DateTime StartTime { get; set; }
    }
}
