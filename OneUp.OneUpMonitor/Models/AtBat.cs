//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneUp.OneUpMonitor.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AtBat
    {
        public int XmlFeedID { get; set; }
        public long GameCode { get; set; }
        public int BattingTeamID { get; set; }
        public int GameSegmentID { get; set; }
        public int Balls { get; set; }
        public int Strikes { get; set; }
        public int Outs { get; set; }
        public int PitcherID { get; set; }
        public int BatterID { get; set; }
        public int OnDeckBatterID { get; set; }
        public int InTheHoleBatterID { get; set; }
        public Nullable<int> Base1RunnerID { get; set; }
        public Nullable<int> Base2RunnerID { get; set; }
        public Nullable<int> Base3RunnerID { get; set; }
        public string PitchSequence { get; set; }
        public Nullable<System.DateTime> GameCastProcessCompleted { get; set; }
        public string TemporaryPlayerData { get; set; }
        public string PitchState { get; set; }
    }
}
