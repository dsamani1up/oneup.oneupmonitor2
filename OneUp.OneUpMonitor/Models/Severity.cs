﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor
{
    public enum Severity
    {
        Debug = 0,
        Info,
        Warning,
        Error,
        Fatal
    }
}
