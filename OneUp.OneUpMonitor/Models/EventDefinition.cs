//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneUp.OneUpMonitor.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class EventDefinition
    {
        public EventDefinition()
        {
            this.EventDefinitionTranslations = new HashSet<EventDefinitionTranslation>();
        }
    
        public int ID { get; set; }
        public Nullable<int> ProcessingSequence { get; set; }
        public string AbbreviatedName { get; set; }
        public string Description { get; set; }
        public int Tier { get; set; }
        public int PointsToBeAwarded { get; set; }
        public string EventType { get; set; }
        public string PlayerPosition { get; set; }
        public Nullable<int> ProviderEventID { get; set; }
        public Nullable<int> ProviderEventDetailID { get; set; }
        public string Keyword { get; set; }
    
        public virtual ICollection<EventDefinitionTranslation> EventDefinitionTranslations { get; set; }
    }
}
