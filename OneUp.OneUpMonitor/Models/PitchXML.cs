﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor
{
    public class Pitch
    {
        public long GameCode { get; set; }
        public DateTime CreatedOn { get; set; }
        public int Sequence { get; set; }
    }
}
