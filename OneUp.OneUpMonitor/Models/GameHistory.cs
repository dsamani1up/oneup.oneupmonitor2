//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OneUp.OneUpMonitor.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class GameHistory
    {
        public int ID { get; set; }
        public string ConnectGameID { get; set; }
        public Nullable<long> GameCode { get; set; }
        public int Season { get; set; }
        public int Period { get; set; }
        public string GameName { get; set; }
        public int TotalParticipants { get; set; }
        public System.DateTime WhenCreated { get; set; }
        public Nullable<int> GameID { get; set; }
    }
}
