﻿using OneUp.OneUpMonitor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor.EmailModels.MLB
{
    public class Update
    {
        public List<Schedule> TodaysGames { get; set; }
        public List<Play> LastFewPlays { get; set; }
        public List<GameEvent> LastFewGameEvents { get; set; }
        public int DataProcessingRestartCount { get; set; }
        public int EventServiceRestartCount { get; set; }
        public bool IsDataProcessingDeltaExceeded { get; set; }
        public bool IsEventServicesDeltaExceeded { get; set; }

    }
}
