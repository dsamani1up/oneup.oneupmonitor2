﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.ServiceProcess;
using OneUp.Shared.Logging;
using System.Threading;
using OneUp.OneUpMonitor.Monitors;
using System.Security;
using System.Security.Permissions;
using System.Reflection;

namespace OneUp.OneUpMonitor
{
    class Program
    {
        static void Main(string[] args)
        {
            int threadSleep = int.Parse(ConfigurationManager.AppSettings["threadSleep"]);

            //Change the AppDomain if default
            SwitchAppDomain();

            //Change console display settings
            Console.WindowWidth = int.Parse(ConfigurationManager.AppSettings["console.Width"]);

            Boolean keepRunning = true;
            MLBMonitor mlbMonitor = new MLBMonitor();
            MLBLegacyMonitor mlbLegacyMonitor = new MLBLegacyMonitor();

            mlbMonitor.RunAsync(threadSleep);
            mlbLegacyMonitor.RunAsync(threadSleep);

            while (keepRunning)
            {
                System.Threading.Thread.Sleep(new TimeSpan(1, 0, 0, 0, 0));
            }

        }

        /// <summary>
        /// This code switches the AppDomain away from the default AppDomain so that RazorEngine can clean up temp files and not throw any errors.
        /// See: https://antaris.github.io/RazorEngine/ for more information
        /// </summary>
        static void SwitchAppDomain()
        {
            if (AppDomain.CurrentDomain.IsDefaultAppDomain())
            {
                AppDomainSetup appDomainSetup = new AppDomainSetup();
                appDomainSetup.ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
                var current = AppDomain.CurrentDomain;
                var domain = AppDomain.CreateDomain("OneUpMonitorAppDomain", null, current.SetupInformation, new PermissionSet(PermissionState.Unrestricted));
                var exitCode = domain.ExecuteAssembly(Assembly.GetExecutingAssembly().Location);
                AppDomain.Unload(domain);
            }
        }
    }
    
    
}
