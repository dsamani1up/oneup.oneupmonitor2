﻿using OneUp.Shared.Logging;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor.Logic
{
    public class CommonLogic
    {
        String serverName = ConfigurationManager.AppSettings["serverName"];
        String className = "CommonLogic";

        public CommonLogic(String _className)
        {
            if(String.IsNullOrWhiteSpace("_className"))
            {
                throw new ArgumentException("_className cannot be null or whitespace.");
            }

            className = _className;
        }

        internal DateTime GetNextInterval(int interval)
        {
            return GetNextInterval(DateTime.Now, interval);
        }

        internal DateTime GetNextInterval(DateTime dateTime, int interval)
        {
            //Divide the current time by the interval and round up to get the multiplier
            Double intervalQuotient = (((double)dateTime.Second / 60) + (double)dateTime.Minute) / (double)interval;
            Double intervialQuotientRounded = Math.Ceiling(intervalQuotient);

            //Remove the minutes portion of the datetime and add in the interval multiplied by the above result
            dateTime = dateTime.Subtract(new TimeSpan(0, dateTime.Minute, dateTime.Second)).AddMinutes(interval * intervialQuotientRounded);

            return dateTime;
        }

        internal void SendEmail(String toEmail, String fromEmail, String subject, String body, Severity severity, Boolean isBodyHtml = false, List<String> ccEmails = null)
        {
            MailMessage message = new MailMessage();
            message.From = new MailAddress(fromEmail);
            message.To.Add(toEmail);
            if(ccEmails != null)
            {
                foreach(String ccEmail in ccEmails)
                {
                    message.CC.Add(ccEmail);
                }
            }

            message.Subject = String.Format("{0} - {1} - {2}", serverName, severity.ToString(), subject);
            message.Body = body;
            message.IsBodyHtml = isBodyHtml;
            SmtpClient client = new SmtpClient();
            client.Host = "smtp.gmail.com";
            client.Port = 587;
            client.EnableSsl = true;
            NetworkCredential smtpCredentials = new NetworkCredential("dsamani@1up.fm", "oneupninja!");
            client.UseDefaultCredentials = false;
            client.Credentials = smtpCredentials;

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {

                Logger(Severity.Error, "SendEmail", ex.ToString());
            }
        }

        internal StartKillTime RestartService(String serviceName)
        {
            StartKillTime skTime = new StartKillTime();

            //Kill service
            ServiceController controller = new ServiceController();
            controller.MachineName = ".";
            controller.ServiceName = serviceName;
            skTime.KillTime = DateTime.Now;
            Log.Instance().InfoFormat("Stopping service at {0}...", skTime.KillTime.ToString());
            try
            {
                controller.Stop();
            }
            catch (Exception ex)
            {
                Logger(Severity.Error, "RestartService", "Unable to stop service {0}", serviceName);
                Logger(Severity.Error, "RestartService", ex.ToString());
            }

            System.Threading.Thread.Sleep(Convert.ToInt32(ConfigurationManager.AppSettings["delayBetweenServiceKillAndStart"]));

            //Start service
            skTime.StartTime = DateTime.Now;
            Log.Instance().InfoFormat("Starting service at {0}...", skTime.StartTime.ToString());
            try
            {
                controller.Start();
            }
            catch (Exception ex)
            {
                Logger(Severity.Error, "RestartService", "Unable to start service {0}", serviceName);
                Logger(Severity.Error, "RestartService", ex.ToString());
            }

            return skTime;
        }

        internal void Logger(Severity severity, String methodName, String message, params object[] formatParams)
        {
            String logLocation = String.Format("{0}.{1}() - ", className, methodName);

            switch(severity)
            {
                case Severity.Debug:
                    Log.Instance().DebugFormat(logLocation + message, formatParams);
                    break;
                case Severity.Info:
                    Log.Instance().InfoFormat(logLocation + message, formatParams);
                    break;
                case Severity.Warning:
                    Log.Instance().WarnFormat(logLocation + message, formatParams);
                    break;
                case Severity.Error:
                    Log.Instance().ErrorFormat(logLocation + message, formatParams);
                    break;
                case Severity.Fatal:
                    Log.Instance().FatalFormat(logLocation + message, formatParams);
                    break;
            }
        }
    }
}
