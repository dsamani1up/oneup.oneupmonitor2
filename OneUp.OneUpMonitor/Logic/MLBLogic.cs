﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RestSharp;
using System.Xml;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.ServiceProcess;
using System.Configuration;
using OneUp.Shared.Logging;
using OneUp.OneUpMonitor.Models;

namespace OneUp.OneUpMonitor.Logic
{
    public class MLBLogic
    {
        CommonLogic commonLogic = new CommonLogic("MLBLogic");
        RestClient mlbDataClient = new RestClient("http://gd2.mlb.com/");

        public List<Schedule> GetGamesInProgress()
        {
            try
            {
                using (var entities = new mlbEntities())
                {
                    return entities.Schedules.Where(i => i.Status == "In-Progress" && i.ProviderEntityID.ToLower() != "sim").ToList();
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetGamesInProgress", ex.ToString());
                return null;
            }
        }

        public Boolean AreGamesInProgress()
        {
            try
            {
                using (var entities = new mlbEntities())
                {
                    return entities.Schedules.Where(i => i.Status == "In-Progress" && i.ProviderEntityID.ToLower() != "sim").Count() > 0;
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "AreGamesInProgress", ex.ToString());
                return false;
            }
        }
        
        /// <summary>
        /// Gets the innings_all.xml file from the MLB based on the provided parameters. Returns the file as a string.
        /// </summary>
        /// <param name="providerGameId"></param>
        /// <param name="startDateTime"></param>
        /// <returns></returns>
        public String GetInningsXml(String providerGameId, DateTime startDateTime)
        {
            try
            {
                //Build URL using the providerGameId
                String year = startDateTime.Year.ToString();
                String month = startDateTime.Month.ToString("D2");
                String day = startDateTime.Day.ToString("D2");
                String url = String.Format("components/game/mlb/year_{0}/month_{1}/day_{2}/gid_{3}/inning/inning_all.xml", year, month, day, providerGameId);

                //Get file
                var request = new RestRequest(url, Method.GET);
                var response = mlbDataClient.Execute(request);

                return response.Content;
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetGame", ex.ToString());
                return null;
            }
        }

        public String GetPlaysXML(String providerGameId, DateTime startDateTime)
        {
            try
            {
                //Build URL using the providerGameId
                String year = startDateTime.Year.ToString();
                String month = startDateTime.Month.ToString("D2");
                String day = startDateTime.Day.ToString("D2");
                String url = String.Format("components/game/mlb/year_{0}/month_{1}/day_{2}/gid_{3}/plays.xml", year, month, day, providerGameId);

                //Get file
                var request = new RestRequest(url, Method.GET);
                var response = mlbDataClient.Execute(request);

                //If info wasn't found, return null
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    return response.Content;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetPlaysXML", ex.ToString());
                return null;
            }

            
        }

        /// <summary>
        /// Gets the delta between a play from the DB and a pitch from the XML
        /// </summary>
        /// <param name="play"></param>
        /// <param name="pitch"></param>
        /// <returns></returns>
        internal Double GetDelta(Play play, Pitch pitch)
        {
            //Subtract the time that the pitch was reported against the time that the play was saved to the DB to get the delta
            var delta = play.DateTimeCreated.Subtract(pitch.CreatedOn).TotalSeconds;
            return delta;
        }

        /// <summary>
        /// Gets the delta between the time a play is created and when a GameEvent processing has started
        /// </summary>
        /// <param name="play"></param>
        /// <param name="gameEvent"></param>
        /// <returns></returns>
        internal Double GetDelta(Play play, GameEvent gameEvent)
        {
            try
            {
                if (gameEvent.ProcessingStarted.HasValue)
                {
                    var delta = gameEvent.ProcessingStarted.Value.Subtract(play.DateTimeCreated).TotalSeconds;
                    return delta;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetDelta", ex.ToString());
                return 0;
            }
        }

        /// <summary>
        /// Gets the delta between the ProcessingFinished and ProcessingStarted properties in the provided GameEvent objectd
        /// </summary>
        /// <param name="gameEvent"></param>
        /// <returns></returns>
        internal Double GetDelta(GameEvent gameEvent)
        {
            try
            {
                var delta = gameEvent.ProcessingFinished.Value.Subtract(gameEvent.ProcessingStarted.Value).TotalSeconds;
                return delta;
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetDelta", ex.ToString());
                return 0;
            }
        }

        /// <summary>
        /// Gets the latest plays from the database for a specified gameCode. 
        /// </summary>
        /// <param name="numberOfPlays"></param>
        /// <param name="gameCode"></param>
        /// <returns></returns>
        public List<Play> GetLatestPlays(int numberOfPlays, long gameCode)
        {
            try
            {
                using (var entities = new mlbEntities())
                {
                    //Return the last few plays in the table (as specified)
                    return entities.Plays.Where(i => i.GameCode == gameCode).OrderByDescending(i => i.DateTimeCreated).Take(numberOfPlays).ToList();
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetLatestPlays", ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Gets the latest plays from the database 
        /// </summary>
        /// <param name="numberOfPlays"></param>
        /// <param name="gameCode"></param>
        /// <returns></returns>
        public List<Play> GetLatestPlays(int numberOfPlays)
        {
            try
            {
                using (var entities = new mlbEntities())
                {
                    //Return the last few plays in the table (as specified)
                    List<Schedule> gamesInProgress = GetGamesInProgress();
                    List<Play> plays = new List<Play>(); 

                    foreach(Schedule game in gamesInProgress)
                    {
                        plays.AddRange(GetLatestPlays(numberOfPlays, game.GameCode));
                    }

                    return plays.OrderByDescending(i => i.DateTimeCreated).ToList();
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetLatestPlays", ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Gets the last pitch that's present in the provided XML string. Skips pitches with type="X"
        /// </summary>
        /// <param name="gameCode"></param>
        /// <param name="inningsXmlString"></param>
        /// <returns></returns>
        internal Pitch GetLastPitchFromXML(long gameCode, string inningsXmlString)
        {
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(inningsXmlString)))
                {
                    //Create the pitchXml object and set some default values
                    Pitch pitchXml = new Pitch();
                    pitchXml.GameCode = gameCode;
                    pitchXml.Sequence = 0;
                    pitchXml.CreatedOn = DateTime.MinValue;

                    //Find the element that has the same sequence ID as the play
                    Boolean isElementFound = false;
                    while (!isElementFound)
                    {
                        //Find the pitch
                        reader.ReadToFollowing("pitch");

                        //Check that this is not an X play
                        reader.MoveToAttribute("type");
                        if (reader.Value.ToLower() == "x")
                        {
                            continue;
                        }

                        //Get the ID/Sequence
                        reader.MoveToAttribute("id");

                        //Check that the value is valid before continuing
                        if (string.IsNullOrWhiteSpace(reader.Value))
                        {
                            isElementFound = true;
                            continue;
                        }

                        int pitchId = 0;
                        Boolean validPitchId = Int32.TryParse(reader.Value, out pitchId);

                        //Check if this pitch's ID attribute is higher than the one saved
                        if (pitchId > pitchXml.Sequence)
                        {
                            //Set the pitchXML sequence
                            pitchXml.Sequence = pitchId;

                            //Get the datetime
                            reader.MoveToAttribute("tfs_zulu");
                            pitchXml.CreatedOn = DateTime.Parse(reader.Value);
                        }
                        else
                        {
                            //If it's not higher, then we've found the last element
                            isElementFound = true;
                        }
                    }

                    //Return the pitchXml object
                    return pitchXml;
                }
            }
            catch (Exception ex)
            {
                Log.Instance().WarnFormat("No pitches found in plays.xml, returning null.");
                return null;
            }
        }

        internal Play GetPlay(long gameCode, decimal sequence)
        {
            using (var entities = new mlbEntities())
            {
                try
                {
                    return entities.Plays.Where(i => i.GameCode == gameCode && i.Sequence == sequence).Single();
                }
                catch (InvalidOperationException)
                {
                    //The play was not found
                    return null;
                }
            }
        }

        /// <summary>
        /// Returns the first GameEvent with a specified PlayId
        /// </summary>
        /// <param name="playId"></param>
        /// <returns></returns>
        internal GameEvent GetGameEvent(int playId)
        {
            using (var entities = new mlbEntities())
            {
                try
                {
                    return entities.GameEvents.Where(i => i.PlayID == playId).First();
                }
                catch (InvalidOperationException)
                {
                    return null;
                }
            }
        }

        internal List<Schedule> GetGamesByDay(DateTime dateTime)
        {
            try
            {
                if (dateTime.TimeOfDay > TimeSpan.FromHours(4))
                {
                    dateTime = dateTime.Date.AddHours(4);
                }
                if (dateTime.TimeOfDay < TimeSpan.FromHours(4))
                {
                    dateTime = dateTime.Date.Subtract(new TimeSpan(1, 0, 0, 0)).AddHours(4);
                }

                DateTime dateTimePlusOne = dateTime.AddDays(1);
                using (var context = new mlbEntities())
                {
                    var obj = context.Schedules.Where(i => i.StartDateTime > dateTime && i.StartDateTime < dateTimePlusOne && i.ProviderEntityID.ToLower() != "sim").ToList();
                    return obj;
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetGamesByDay", ex.ToString());
                return null;
            }
        }

        internal string GetGameStatus(string playsXml)
        {
            try
            {
                using (XmlReader reader = XmlReader.Create(new StringReader(playsXml)))
                {
                    //Find the game node
                    reader.ReadToFollowing("game");

                    //Find the status attribute
                    reader.MoveToAttribute("status");

                    //Return the status value
                    return reader.Value;
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetGameStatus", ex.ToString());
                return null;
            }
        }

        internal Schedule GetGame(long gameCode)
        {
            try
            {
                using (var entities = new mlbEntities())
                {
                    return entities.Schedules.Where(i => i.GameCode == gameCode).Single();
                }
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "GetGame", ex.ToString());
                return null;
            }
        }

        internal void LogDeltaToCSV(String providerGameID, long gameCode, Decimal sequence, Double delta)
        {
            String fieldDelimiter = @",";
            String recordDelimiter = @"\n";

            StringBuilder recordBuilder = new StringBuilder();
            recordBuilder.Append(providerGameID + fieldDelimiter);
            recordBuilder.Append(gameCode + fieldDelimiter);
            recordBuilder.Append(sequence + fieldDelimiter);
            recordBuilder.Append(delta + recordDelimiter);

            try
            {
                String csvFolderPath = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings["mlb.DeltaCSVFolderPath"];
                String folderPath = Directory.CreateDirectory(csvFolderPath).FullName;
                String fileName = DateTime.Now.ToString("YYYY-MM-DD") + ".csv";

                File.AppendAllText(Path.Combine(folderPath, fileName), recordBuilder.ToString());
            }
            catch (Exception ex)
            {
                commonLogic.Logger(Severity.Error, "LogDeltaToCSV", ex.ToString());
            }
        }
    }
}
