﻿using OneUp.OneUpMonitor.EmailModels.MLB;
using OneUp.OneUpMonitor.Models;
using RazorEngine;
using RazorEngine.Templating;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneUp.OneUpMonitor.Logic
{
    public class MLBEmails
    {
        CommonLogic commonLogic = new CommonLogic("MLBEmails");
        //TemplateService templateService = new TemplateService();
        //RazorEngineService razorService = new RazorEngineService();
        String templateFolderPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "EmailTemplates/MLB");
        String fromEmail = ConfigurationManager.AppSettings["fromEmail"];
        String supportEmail = ConfigurationManager.AppSettings["supportEmail"];

        public MLBEmails()
        {
            CacheTemplates();
        }

        internal void CacheTemplates()
        {
            Engine.Razor.AddTemplate("mlb.DataProcessingRestarted", File.ReadAllText(templateFolderPath + "/DataProcessingRestarted.cshtml"));
            Engine.Razor.AddTemplate("mlb.Update", File.ReadAllText(templateFolderPath + "/Update.cshtml"));
            Engine.Razor.AddTemplate("mlb.GameStarted", File.ReadAllText(templateFolderPath + "/GameStarted.cshtml"));
            Engine.Razor.AddTemplate("mlb.GameEnded", File.ReadAllText(templateFolderPath + "/GameEnded.cshtml"));
            Engine.Razor.AddTemplate("mlb.GameFailedToStart", File.ReadAllText(templateFolderPath + "/GameFailedToStart.cshtml"));
            Engine.Razor.AddTemplate("mlb.GameFailedToEnd", File.ReadAllText(templateFolderPath + "/GameFailedToEnd.cshtml"));
            Engine.Razor.AddTemplate("mlb.DataProcessingRestarted", File.ReadAllText(templateFolderPath + "/DataProcessingRestarted.cshtml"));
            Engine.Razor.AddTemplate("mlb.EventServiceNodesRestarted", File.ReadAllText(templateFolderPath + "/EventServicesNodesRestarted.cshtml"));
        }

        internal void SendDataProcessingRestartedEmail(StartKillTime skTime)
        {
            String subject = "DataProcessing.MLB Service Restarted";
            //String body = String.Format("The DataProcessing service exceeded a maximum allowed time difference between MLB data and OneUp data. \nService was killed at {0} and started again at {1}. \nThe next check will be in 10 minutes.", skTime.KillTime, skTime.StartTime);
            //String body = templateService.Parse(File.ReadAllText(templateFolderPath + "/DataProcessingRestarted.cshtml"), skTime, null, "DataProcessingRestarted");
            String body = Engine.Razor.RunCompile("mlb.DataProcessingRestarted", null, skTime);
            commonLogic.SendEmail(supportEmail, fromEmail, subject, body, Severity.Warning, true);
        }

        internal void SendEventServiceNodesRestartedEmail(StartKillTime skTime)
        {
            String subject = "EventService Nodes Restarted";
            //String body = String.Format("The EventService process exceeded a maximum allowed time difference between received plays and processed events. \nService was killed at {0} and started again at {1}. \nThe next check will be in 10 minutes.", skTime.KillTime, skTime.StartTime);
            //String body = templateService.Parse(File.ReadAllText(templateFolderPath + "/EventServiceNodesRestarted.cshtml"), skTime, null, "EventServiceNodesRestarted");
            String body = Engine.Razor.RunCompile("mlb.EventServiceNodesRestarted", null, skTime);
            commonLogic.SendEmail(supportEmail, fromEmail, subject, body, Severity.Warning, true);
        }

        internal void SendGameFailedToStartEmail(Schedule game)
        {
            String subject = "MLB game failed to start";
            String body = Engine.Razor.RunCompile("mlb.GameFailedToStart", null, game);
            commonLogic.SendEmail(supportEmail, fromEmail, subject, body, Severity.Error, true);
        }

        internal void SendGameFailedToEndEmail(Schedule game, DateTime failedTime)
        {
            String subject = "MLB game failed to end";
            String body = Engine.Razor.RunCompile("mlb.GameFailedToEnd", null, game);
            commonLogic.SendEmail(supportEmail, fromEmail, subject, body, Severity.Error, true);
        }

        internal void SendGameStartedEmail(Schedule game)
        {
            String subject = "MLB game successfully started";
            String body = Engine.Razor.RunCompile("mlb.GameStarted", null, game);
            commonLogic.SendEmail(supportEmail, fromEmail, subject, body, Severity.Info, true);
        }

        internal void SendGameEndedEmail(Schedule game)
        {
            String subject = "MLB game successfully ended";
            String body = Engine.Razor.RunCompile("mlb.GameEnded", null, game);
            commonLogic.SendEmail(supportEmail, fromEmail, subject, body, Severity.Info, true);
        }

        internal void SendUpdateEmail(Update mlbUpdate)
        {
            String to = "mbodenheimer@1up.fm";
            List<String> ccEmails = new List<string>()
            {
                "dsamani@1up.fm"
            };
            String subject = "MLB Update Email";

            String body = Engine.Razor.RunCompile("mlb.Update", null, mlbUpdate);
            commonLogic.SendEmail(to, fromEmail, subject, body, Severity.Info, true, ccEmails);
        }
    }
}

